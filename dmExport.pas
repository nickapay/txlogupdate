(************************************************************************************************************************************

                      SOUNDMOUSE MUSIC RETURNS REPORT GENERATOR

Date:				|	Author:	     |	Version:	|	Change:
05 Jan 2004                     |       Nick Pay     |  1.0.0.0         |       Original Development
21 Jan 2008                     |       Nick Pay     |  1.0.0.1         |       Added MCPSPRS to update flags
22 Jan 2008                     |       Nick Pay     |  1.0.0.2         |       Added DPRS / IMRO
************************************************************************************************************************************)

unit dmExport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Oracle, StdCtrls, ExtCtrls, IdGlobal;

const
  gconOracleNLSDateFormat = 'DD-MM-YYYY HH24:MI:SS';
  gconShortDate = 'dd/mm/yyyy';
  gconSimpleDate = 'ddmmyy';
  gconShortOracleDateFormat = 'DD/MM/YYYY';
  gconDateFormat = 'dd-mmm-yyyy hh:mm:ss';
  gconLongTime = 'HH:NN:SS';
  gconNullTime = ' 00:00:00';
  gconNullDate = '01/01/2000';
  gconNullDate2 = '01-Jan-2000';
  gconDateFormatForFile = 'YYYYMMDD';
  gconSent = 'ST';

  { Cuesheet status }
  gconIssued = 'IS';
  gconToBeIssued = 'TI';
  gconApproved = 'AP';
  gconReApproved = 'RE';
  gconSubmitted = 'SB';
  gconInProgress = 'IP';

  { Music Origins }
  gconMOCommissioned = 'X';
  gconMOLibrary = 'L';
  gconMOCommercial = 'C';
  gconMOVideo = 'V';
  gconMOLive = 'P';
  gconMOSoundtrack = 'T';
  gconMOStudioRecording = 'R';

  gconNoMusic = 'NM';
  gconProduction = 'N';
  gconCommissioned = 'C';
  gconInHouse = 'H';
  gconAcquired = 'A';

type
  TExport = class(TForm)
    Button1: TButton;
    OracleLogon1: TOracleLogon;
    SessionCUE: TOracleSession;
    qryReport: TOracleQuery;
    qryGen: TOracleQuery;
    Memo1: TMemo;
    eChannelID: TEdit;
    Label1: TLabel;
    qryRollback: TOracleQuery;
    qryCommit: TOracleQuery;
    eDatabase: TEdit;
    Label2: TLabel;
    OpenDialog1: TOpenDialog;
    cbCommit: TCheckBox;
    rgAgency: TRadioGroup;
    eUpdateRowCount: TEdit;
    Label3: TLabel;
    qryCheck: TOracleQuery;
    Button5: TButton;
    eDateReported: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    lblVersion: TLabel;
    Button6: TButton;
    cbUseFileNameDate: TCheckBox;
    cbRunUpdates: TCheckBox;
    procedure Button1Click(Sender: TObject);
    procedure upSetDatabaseDateSession;
    procedure upRollback;
    procedure upCommit;
    function ufDbConnect(DatabaseName:string): boolean;
    procedure upSetRollbackSegment;
    procedure upRunUpdateRoutine;
    procedure upRunSeriesUpdateRoutine;
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure btnPPLExcludeClick(Sender: TObject);
    procedure upConnectToDatabase;
    function  ufMyTrim(sValue: string):string;
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    function WriteToLog(sLogMessage: string): Boolean;
  private
    { Private declarations }
    function gfCheckFirstTxOfDay(DateTimeTransmit, DateTransmit, ProductionNo, ChannelID: string): boolean;
  public
    { Public declarations }
  end;

var
  Export: TExport;

implementation

{$R *.dfm}

procedure TExport.upConnectToDatabase;
begin
  ufdbConnect(eDatabase.Text);
  Memo1.Lines.Add(' Connected to database');
  upRollback;
  upSetRollbackSegment;
  Memo1.Lines.Add(' Rollback Segment set');
  upSetDatabaseDateSession;
  Memo1.Lines.Add(' Date Session set');
end;

procedure TExport.upSetDatabaseDateSession;
begin
  try  { try except }
    { Set those pesky global date formats }
    ShortDateFormat :=  gconShortOracleDateFormat;
    LongTimeFormat := gconLongTime;

    with qryGen do
    begin
      Close;
      SQL.Clear;
      SQL.Add(Format('alter session set NLS_DATE_FORMAT = %s', [QuotedStr(gconOracleNLSDateFormat)]));
      Execute;
      Close;
    end;
  except
    on E: Exception do
    begin
      ShowMessage('- WARN: - Error in setting database date format :' +E.Message);
    end;
  end;
end;


procedure TExport.upRollback;
begin
  with qryRollback do
  begin
    try
      Close;
      SQL.Clear;
      SQL.Add('rollback');
      Execute;
      Close;
    except
    end;
  end;
end;

procedure TExport.upCommit;
begin
  with qryCommit do
  begin
    try
      Close;
      SQL.Clear;
      SQL.Add('commit');
      Execute;
      Close;
    except
      on E: Exception do
      begin
        ShowMessage('- STOP: - TdmExport.upCommit, Error occured commiting data :'+E.Message);
        Raise;
      end;
    end;
  end;
end;

function TExport.ufDbConnect(DatabaseName:string): boolean;
begin
  Result := True;
  if not(SessionCUE.Connected) then
  begin
    try
      SessionCUE.LogonDatabase := DatabaseName;
      SessionCUE.LogOn;
      //successful connection - so flag as having been connected today.
      //ustrDateProcessed := FormatDateTime('DD-MM-YYYY', Now);
    except
      on E: Exception do
      begin
        Result := False;
        ShowMessage('- STOP: - TdmExport.ufDbConnect, DB could not be connected to: ' + E.Message);
      end;
    end;
  end;
end;

procedure TExport.upSetRollbackSegment;
begin
  try  { try except }
    { Set those pesky global date formats }
    with qryGen do
    begin
      Close;
      SQL.Clear;
      SQL.Add('Set transaction use rollback Segment RL01');
      Execute;
      Close;
    end;
  except
    on E: Exception do
    begin
      ShowMessage('- WARN: - Error in setting rollback segment :' +E.Message);
    end;
  end;
end;

function TExport.ufMyTrim(sValue: string):string;
var
  setNums : set of '0'..'9';
  sResult : string;
  i : integer;
begin
  setNums := ['0'..'9'];
  for i := 0 to length(sValue) do
  begin
    if sValue[i] in setNums then
      sResult := sResult + sValue[i];
  end;
  Result := sResult;
end;


function TExport.WriteToLog(sLogMessage: string): Boolean;
var
  sMessage,
  lstrLogDate,
  sLogFileName: string;
  LogFile : TextFile;
begin
  Result := False;
  lstrLogDate := FormatDateTime(gconDateFormat,Now);
  sLogFileName := FormatDateTime('YYYYMMDD', Now)+'IMPORT.log';
  sMessage := lstrLogDate+' : '+sLogMessage;
  try
    AssignFile(LogFile, sLogFileName);
    if FileExists(sLogFileName) then
    begin
      Append(LogFile);
    end else
    begin
      ReWrite(LogFile);
    end;
    Writeln(LogFile, sMessage);
    Result := True;
  finally
    CloseFile(LogFile);
  end;

end;


procedure TExport.upRunUpdateRoutine;
var
  UpdateTextFile : TextFile;
  lstrTxLogID : string;
  lintCount : integer;
  srImport: TSearchRec;
  sDateReported : string;
  lstrlUpdateList : TStringList;
  lstrlTxlogStatusCheck : TStringList;
  lstrLogonPrefix, lstrBroadcasterPrefix : string;
  lstrBroadcasterName, lstrChannelName : string;
  booFileValid : boolean;
  lstTxlogupdateList : Tlist;
  lintTxlogCount : integer;
  lintTListCounter : integer;

begin

  if OpenDialog1.Execute then
  try

    if (FindFirst('*.txt',faAnyFile,srImport)=0) then
    begin
      repeat
        try
          lstrlTxlogStatusCheck := TStringList.Create;
          lstrlTxlogStatusCheck.Sorted := True;
          lstrlTxlogStatusCheck.Duplicates := dupIgnore;
          lstrlTxlogStatusCheck.Clear;

          AssignFile(UpdateTextFile, srImport.Name);
          Reset(UpdateTextFile);
          lstrBroadcasterPrefix := Copy(srImport.Name,Pos('_',srImport.Name)+1,3);
          if cbUseFileNameDate.Checked = True then
          begin
            //MCPS_TXLOGUPDATE_20-Apr-2010.log
            sDateReported := Copy(srImport.Name,Pos('Update',srImport.Name)+7,8);
            sDateReported := Copy(sDateReported,7,2)+'/'+Copy(sDateReported,5,2)+'/'+Copy(sDateReported,1,4);
          end else
          begin
            sDateReported := eDateReported.Text;
          end;

          lintTxlogCount := 0;
          lstTxlogupdateList := TList.Create;
          booFileValid := True;

          while not EOF(UpdateTextFile) do
          begin
            Screen.Cursor := crHourGlass;
            Readln(UpdateTextFile, lstrTxLogID);

            lstrlTxlogStatusCheck.Add(lstrTxLogID);
            inc(lintTxlogCount);
            if lintTxlogCount = 500 then
            begin
              lstTxlogUpdateList.Add(lstrlTxlogStatusCheck);
              lintTxlogCount := 0;

              lstrlTxlogStatusCheck := TStringList.Create;
              lstrlTxlogStatusCheck.Sorted := True;
              lstrlTxlogStatusCheck.Duplicates := dupIgnore;
            end;
          end;
          //get the remaining batch of update ID's
          if lintTxlogCount > 0 then
            lstTxlogUpdateList.Add(lstrlTxlogStatusCheck);

          for lintTListCounter := 0 to lstTxlogupdateList.Count-1 do
          begin

            with qryCheck do
            try
              Close;
              SQL.Clear;
              SQL.Add(Format(' select distinct CHANNEL_ID from CUE.TXLOG where ID in (%s) ',
              [
                TStringList(lstTxlogupdateList[lintTListCounter]).CommaText
              ]
              ));
              Execute;

              while not EOF do
              begin
                with qryReport do
                try
                  Close;
                  SQL.Clear;
                  SQL.Add(Format(' select CH.NAME CHANNEL , B.NAME BROADCASTER, B.LOGON_PREFIX '+
                  ' from CUE.BROADCASTER B, CUE.CHANNEL CH '+
                  ' where CH.BROADCASTER_ID = B.ID '+
                  ' and CH.ID = %s ',
                  [
                    qryCheck.FieldAsString('CHANNEL_ID')
                  ]));
                  Execute;
                  while not EOF do
                  begin
                    lstrLogonPrefix := FieldAsString('LOGON_PREFIX');
                    lstrChannelName := FieldAsString('CHANNEL');
                    lstrBroadcasterName := FieldAsString('BROADCASTER');

                    if lstrLogonPrefix = lstrBroadcasterPrefix then
                    begin
                      Memo1.Lines.Add(lstrChannelName+' is a channel for '+lstrBroadcasterName);
                      WriteToLog(lstrChannelName+' is a channel for '+lstrBroadcasterName);
                    end else
                    begin
                      booFileValid := False;
                      Memo1.Lines.Add(lstrChannelName+' is NOT a channel for '+lstrLogonPrefix+ ' It belongs to '+lstrBroadcasterName);
                      WriteToLog(lstrChannelName+' is NOT a channel for '+lstrLogonPrefix+ ' It belongs to '+lstrBroadcasterName);
                      CopyFileTo(srImport.Name,ExtractFileDir(srImport.Name)+'\rejected\'+ExtractFileName(srImport.Name));
                      //Exception.Create(lstrChannelName+' is NOT a channel for '+lstrLogonPrefix+ ' It belongs to '+lstrBroadcasterName);
                    end;
                    Next;
                  end;
                except
                  on E: Exception do
                    Raise;
                end;
                Next;
              end;
            except
              on E: Exception do
              begin
                Memo1.Lines.Add('Error : Txlog Data data check for '+lstrBroadcasterPrefix+' '+E.Message);
                WriteToLog('Error : Txlog Data data check for '+lstrBroadcasterPrefix+' '+E.Message);
                ShowMessage('ERROR: - Incorrect Txlog ID for this broadcaster: '+E.Message);
              end;
            end;
          end;

          if (booFileValid = True) and (cbRunUpdates.Checked = True) then
          begin
            try  { try except }
              lstrlUpdateList := TStringList.Create;
              for lintTListCounter := 0 to lstTxlogupdateList.Count-1 do
              begin
                lstrlUpdateList.Assign(TStringList(lstTxlogUpdateList[lintTListCounter]));
                with qryGen do
                begin
                  Close;
                  SQL.Clear;
                  SQL.Add('        update CUE.TXLOG set ');
                  if rgAgency.ItemIndex = 0 then
                  begin
                    SQL.Add(Format(' CC_MCPS_REPORT_STATUS = %s ',[QuotedStr(gconSent)]));
                    SQL.Add(Format(',DATE_REPORTED_TO_MCPS = %s',[QuotedStr(sDateReported)]));
                  end;
                  if rgAgency.ItemIndex = 1 then
                    SQL.Add(Format(' CC_PRS_REPORT_STATUS = %s ',[QuotedStr(gconSent)]));
                  if rgAgency.ItemIndex = 2 then
                  begin
                    SQL.Add(Format(' CC_MCPSPRS_REPORT_STATUS = %s ',[QuotedStr(gconSent)]));
                    SQL.Add(Format(',DATE_REPORTED_TO_MCPSPRS = %s',[QuotedStr(sDateReported)]));
                  end;
                  if rgAgency.ItemIndex = 3 then
                  begin
                    SQL.Add(Format(' CC_PPL_REPORT_STATUS = %s ',[QuotedStr(gconSent)]));
                    SQL.Add(Format(',DATE_REPORTED_TO_PPL = %s',[QuotedStr(sDateReported)]));
                  end;
                  if rgAgency.ItemIndex = 4 then
                  begin
                    SQL.Add(Format(' CC_VPL_REPORT_STATUS = %s ',[QuotedStr(gconSent)]));
                    SQL.Add(Format(',DATE_REPORTED_TO_VPL = %s',[QuotedStr(sDateReported)]));
                  end;
                  if rgAgency.ItemIndex = 5 then
                  begin
                    SQL.Add(Format(' CC_STIM_REPORT_STATUS = %s ',[QuotedStr(gconSent)]));
                    SQL.Add(Format(',DATE_REPORTED_TO_STIM = %s',[QuotedStr(sDateReported)]));
                  end;
                  if rgAgency.ItemIndex = 6 then
                  begin
                    SQL.Add(Format(' CC_SIAE_REPORT_STATUS = %s ',[QuotedStr(gconSent)]));
                    SQL.Add(Format(',DATE_REPORTED_TO_SIAE = %s',[QuotedStr(sDateReported)]));
                  end;
                  if rgAgency.ItemIndex = 7 then
                  begin
                    SQL.Add(Format(' CC_SCF_REPORT_STATUS = %s ',[QuotedStr(gconSent)]));
                    SQL.Add(Format(',DATE_REPORTED_TO_SCF = %s',[QuotedStr(sDateReported)]));
                  end;
                  if rgAgency.ItemIndex = 8 then
                  begin
                    SQL.Add(Format(' CC_AFI_REPORT_STATUS = %s ',[QuotedStr(gconSent)]));
                    SQL.Add(Format(',DATE_REPORTED_TO_AFI = %s',[QuotedStr(sDateReported)]));
                  end;
                  if rgAgency.ItemIndex = 9 then
                  begin
                    SQL.Add(Format(' CC_DPRS_REPORT_STATUS = %s ',[QuotedStr(gconSent)]));
                    SQL.Add(Format(',DATE_REPORTED_TO_DPRS = %s',[QuotedStr(sDateReported)]));
                  end;
                  if rgAgency.ItemIndex = 10 then
                  begin
                    SQL.Add(Format(' CC_IMRO_REPORT_STATUS = %s ',[QuotedStr(gconSent)]));
                    SQL.Add(Format(',DATE_REPORTED_TO_IMRO = %s',[QuotedStr(sDateReported)]));
                  end;

                  SQL.Add(Format(' where ID in (%s) ',
                  [
                    TStringList(lstTxlogupdateList[lintTListCounter]).CommaText
                  ]));
                  Execute;
                  Application.ProcessMessages;
                  Close;
                  eUpdateRowCount.Text := IntToStr(lstrlTxlogStatusCheck.Count);

                end;
              end; // for loop

            except
              on E: Exception do
              begin
                ShowMessage('- WARN: - : '+E.Message);
                Memo1.Lines.Add('Error : Rollback of ' +srImport.Name +' due to '+E.Message);
                WriteToLog('Error : Rollback of ' +srImport.Name +' due to '+E.Message);
                upRollback;
              end;
            end;
          end;

          if cbCommit.Checked = True then
          begin
            upCommit;
            Memo1.Lines.Add('File update commit : ' +srImport.Name);
          end else
          begin
            upRollback;
            Memo1.Lines.Add('Rollback : ' +srImport.Name);
          end;
        finally
          for lintTListCounter := 0 to lstTxlogupdateList.Count-1 do
          begin
            TStringList(lstTxlogUpdateList[lintTListCounter]).Free;
          end;
          lstTxlogUpdateList.Free;
          lstrlUpdateList.Free;
        end;

      until FindNext(srImport) <> 0;
      FindClose(srImport);

    end;


    Memo1.Lines.Add('Txlog ID updated for '+OpenDialog1.FileName);

  finally
    CloseFile(UpdateTextFile);
    Screen.Cursor := crDefault;
    SessionCUE.LogOff;
  end;
end;

procedure TExport.upRunSeriesUpdateRoutine;
var
  UpdateTextFile : TextFile;
  lstrTxLogID : string;
  lintCount : integer;

begin
  if OpenDialog1.Execute then
  try
    AssignFile(UpdateTextFile, OpenDialog1.FileName);
    Reset(UpdateTextFile);
    while not EOF(UpdateTextFile) do
    begin
      Screen.Cursor := crHourGlass;
      Readln(UpdateTextFile, lstrTxLogID);

      try  { try except }
        with qryGen do
          begin
          Close;
          SQL.Clear;
          SQL.Add(Format(' update CUE.TXSERIES set CC_PRODUCTION_SOURCE = %s ',[QuotedStr(gconCommissioned)]));
          SQL.Add(Format(' where ID = %s ',[trim(lstrTxLogID)]));
          Execute;
          Application.ProcessMessages;
          Inc(lintCount);
          eUpdateRowCount.Text := IntToStr(lintCount);
          Close;
        end;
      except
        on E: Exception do
        begin
          ShowMessage('- WARN: - : '+E.Message);
          upRollback;
        end;
      end;
    end;
    ShowMessage('TxSeries Production Source updated for '+OpenDialog1.FileName);
    if cbCommit.Checked = True then
      upCommit
    else
      upRollback;
  finally
    CloseFile(UpdateTextFile);
    Screen.Cursor := crDefault;
    SessionCUE.LogOff;
  end;
end;


function TExport.gfCheckFirstTxOfDay(DateTimeTransmit, DateTransmit,
  ProductionNo, ChannelID: string): boolean;
var
  lstrMinDate : string;
  lstrDayBeforeDateTransmit : string;
begin
//  { Date to check will only be the current DateTransmit and the previous day when the time is past midnight }
//  lstrDayBeforeDateTransmit :=  FormatDateTime('DD/MM/YYYY', IncDay(StrToDate(DateTransmit), -1));
  with qryCheck do
  try
    Close;
    SQL.Clear;
    SQL.Add('        select MIN(DATE_TIME_TRANSMIT) as MINDATE from CUE.TXLOG TXL, CUE.TXPROGRAMME TXP ');
    SQL.Add(Format(' where PRODUCTION_NO = %s ',[QuotedStr(ProductionNo)]));
    SQL.Add('        and TXL.TXPROGRAMME_ID = TXP.ID ');
    SQL.Add(Format(' and TXL.DATE_TRANSMIT = %s',[QuotedStr(DateTransmit)]));
    SQL.Add(Format(' and CHANNEL_ID = %s',[ChannelID]));
    Execute;
    if not EOF then
    begin
      lstrMinDate := FieldAsString('MINDATE');
      if lstrMinDate = DateTimeTransmit then
        Result := True
      else
        Result := False;
    end else
    begin
      ShowMessage('- WARN: -  gfCheckFirstTxOfDay : no value found for date_time_transmit');
      Result := False;
    end;
  except
    On E:Exception do
    begin
      ShowMessage('- WARN: -  gfCheckFirstTxOfDay ' + E.Message);
      Result := False;
    end;
  end;


end;


procedure TExport.Button1Click(Sender: TObject);
begin
  if rgAgency.ItemIndex <> -1 then
  begin
    upConnectToDatabase;
    upRunUpdateRoutine;
  end else
    ShowMessage('An Agency must be selected');
end;



procedure TExport.Button2Click(Sender: TObject);
var
  XLTextFile : TextFile;
  lstrTxLogID : string;
  lintCount : integer;
  lstrDateTimeTransmit, lstrDateTransmit, lstrDateFirstTransmit,
  lstrType, lstrProductionNo, lstrChannelID : string;
  lintFirstTx, lintMCPSUnits,
  lintSecDuration : integer;
  lboolFirstTx : boolean;
  lstrProductionSource,
  lstrEpisodeName,
  lstrSeriesName,
  lstrOrigin : string;
begin
  { Using the view created in the Exports application, run through the dataset and create a csv file
    detailing the channel, music origin, Transmission data, Track duration, First TX as calculated by looking at the dates }
  upConnectToDatabase;
  lintCount := 0;
  if OpenDialog1.Execute then
  try
    AssignFile(XLTextFile, OpenDialog1.FileName);
    Rewrite(XLTextFile);
    writeln(XLTextFile, 'ChannelID,MusicOrigin,Prog Type,DateTransmit,Duration,DateFirstTx,ProductionNo,SeriesName,EpisodeName,MCPSUnits,FirstTx,Prod Source');
    with qryGen do
    begin
      Close;
      SQL.Clear;
      SQL.Add(' select CHANNELID, DATE_FIRST_TX, DATE_TRANSMIT, DATE_TIME_TRANSMIT, CC_ORIGIN, CC_TYPE, SERIES_NAME, ');
      SQL.Add(' (TO_NUMBER(SUBSTR(DATE_DURATION,12,2))*3600)+ ');
      SQL.Add(' (TO_NUMBER(SUBSTR(DATE_DURATION,15,2))*60)+ ');
      SQL.Add(' (TO_NUMBER(SUBSTR(DATE_DURATION,18,2))) as SEC_DURATION, PRODUCTION_NO , EPISODE_NAME, SX_PROD_SOURCE ');
      SQL.Add(' from cue.vw_primary_exportreport where ');
      SQL.Add(Format('  CC_STATUS IN (%s, %s)',[QuotedStr(gconApproved), QuotedStr(gconReApproved)]));
      SQL.Add(Format( ' and CC_MUSIC_STATUS <> %s',[QuotedStr(gconNoMusic)]));
      SQL.Add(Format( ' and NOT (CC_TYPE <> %s and CC_ORIGIN = %s) ',[QuotedStr(gconProduction), QuotedStr(gconMOLibrary)]));
      SQL.Add(Format( ' and CC_ORIGIN in (%s, %s, %s)',[QuotedStr(gconMOCommercial), QuotedStr(gconMOLibrary), QuotedStr(gconMOVideo)]));
      SQL.Add(Format( ' and SX_PROD_SOURCE in (%s, %s)',[QuotedStr(gconCommissioned), QuotedStr(gconInHouse)]));
      SQL.Add('         order by channelid, date_transmit ');
      Execute;
      while not EOF do
      begin
        lstrDateTransmit := FieldAsString('DATE_TRANSMIT');
        lstrDateTimeTransmit := FieldAsString('DATE_TIME_TRANSMIT');
        lstrDateFirstTransmit := FieldAsString('DATE_FIRST_TX');
        lstrProductionNo := FieldAsString('PRODUCTION_NO');
        lstrChannelID := FieldAsString('CHANNELID');
        lintSecDuration := FieldAsInteger('SEC_DURATION');
        lstrOrigin := FieldAsString('CC_ORIGIN');
        lintMCPSUnits := lintSecDuration div 30;
        lstrType := FieldAsString('CC_TYPE');
        lstrProductionSource := FieldAsString('SX_PROD_SOURCE');
        lstrSeriesName := FieldAsString('SERIES_NAME');
        lstrEpisodeName := FieldAsString('EPISODE_NAME');
        if (lintSecDuration mod 30) <> 0 then
          Inc(lintMCPSUnits); //the remainder goes up

        if (lstrDateTransmit = lstrDateFirstTransmit) and gfCheckFirstTxOfDay(lstrDateTimeTransmit, lstrDateTransmit, lstrProductionNo, lstrChannelID) then
          lboolFirstTx := True
        else
          lboolFirstTx := False;

        if lboolFirstTx then
        begin
          if lstrOrigin = gconMOLibrary then
            lintMCPSUnits := lintMCPSUnits * 3;
          lintFirstTx := 1;
        end
        else
          lintFirstTx := 0;

        if ((lstrOrigin = gconMOCommercial) and (lboolFirstTx = True)) or (lstrOrigin = gconMOlibrary) or (lstrOrigin = gconMOVideo) then
          writeln(XLTextFile, lstrChannelID+','+lstrOrigin+','+lstrType+','+lstrDateTransmit+','+IntToStr(lintSecDuration)+','+lstrDateFirstTransmit+','+lstrProductionNo+','+lstrSeriesName+','+lstrEpisodeName+','+IntToStr(lintMCPSUnits)+','+IntToStr(lintFirstTx)+','+lstrProductionSource);

        Next;
      end;
    end;
  finally
    CloseFile(XLTextFile);
  end;

  if OpenDialog1.Execute then
  try
    AssignFile(XLTextFile, OpenDialog1.FileName);
    Rewrite(XLTextFile);
    with qryGen do
    begin
      Close;
      SQL.Clear;
      SQL.Add(' select * ');
      SQL.Add(' from cue.vw_PRIMARY_AGGR_exportreport ');
      SQL.Add('         order by channelid, date_transmit ');
      Execute;
      while not EOF do
      begin
        lstrDateTransmit := FieldAsString('DATE_TRANSMIT');
        lstrProductionNo := FieldAsString('PRODUCTION_NO');
        lstrChannelID := FieldAsString('CHANNELID');
        lintSecDuration := FieldAsInteger('TOTTIME');
        lintMCPSUnits := lintSecDuration div 30;
        lstrType := FieldAsString('CC_TYPE');
        if (lintSecDuration mod 30) <> 0 then
          Inc(lintMCPSUnits); //the remainder goes up

        writeln(XLTextFile, lstrChannelID+','+IntToStr(lintSecDuration)+','+lstrDateTransmit+','+lstrProductionNo+','+lstrType+','+IntToStr(lintMCPSUnits));
        Next;
      end;
    end;
  finally
    CloseFile(XLTextFile);
  end;
end;

procedure TExport.Button3Click(Sender: TObject);
begin
  upConnectToDatabase;
  upRunSeriesUpdateRoutine;
end;

procedure TExport.btnPPLExcludeClick(Sender: TObject);
var
  lstrCuesheetID : string;
  UpdateTextFile : TextFile;
  lintCuesheetID : integer;
begin
  upConnectToDatabase;
  if OpenDialog1.Execute then
  try
    AssignFile(UpdateTextFile, OpenDialog1.FileName);
    Reset(UpdateTextFile);
    while not EOF(UpdateTextFile) do
    begin
      Screen.Cursor := crHourGlass;
      Readln(UpdateTextFile, lstrCuesheetID);
      lstrCuesheetId := ufMyTrim(lstrCuesheetID);
      try
        lintCuesheetID := StrToInt(lstrCuesheetID);
      except
        ShowMessage('invalid cuesheet id ');
      end;
      with qryReport do
      try
        Close;
        SQL.Clear;
        SQL.Add(Format(' Update CUE.CUES set BIT_PPL_EXEMPT = 1 where CUESHEET_ID = %s ',[lstrCuesheetID]));
        Execute;
      except
        on E: Exception do
        begin
          ShowMessage('- WARN: - : '+E.Message);
          upRollback;
        end;
      end;
    end;
    ShowMessage('PPL Exemption flags set ');
    if cbCommit.Checked = True then
      upCommit
    else
      upRollback;
  finally
    CloseFile(UpdateTextFile);
    Screen.Cursor := crDefault;
    SessionCUE.LogOff;
  end;
end;

procedure TExport.Button4Click(Sender: TObject);
var
  MCPSMapFile : TextFile;
  MyStringList : TStringList;
  lstrfield, lstrmatch : string;
  bQuote : boolean;
  i, j : integer;
  lstrMatchLong : string;
begin
  upConnectToDatabase;
  if OpenDialog1.Execute then
  try
    AssignFile(MCPSMapFile, OpenDialog1.FileName);
    Reset(MCPSMapFile);
    MyStringList := TStringList.Create;
    while not EOF(MCPSMapFile) do
    begin
      mystringlist.Clear;
      bQuote := false;
      Screen.Cursor := crHourGlass;
      Readln(MCPSMapFile, lstrMatchLong);
      lstrMatch := Trim(lstrMatchLong);
      lstrfield := '';
      for i := 1 to length(lstrMatch) do
      begin
        if lstrMatch[i] = '"' then
          bQuote := bquote xor true
        else if (lstrmatch[i] = ',') and (bQuote = false) then
        begin
          mystringlist.add(lstrfield);
          lstrfield := '';
        end
        else lstrfield := lstrfield+lstrmatch[i];
      end;
      mystringlist.add(lstrfield);
//        MyStringList[i] := copy(1,pos(',',lstrMatch)-1);
//        lstrMatch := copy(pos(',',lstrMatch)+1,lstrMatch.length);
//      end;
//      MyStringList.CommaText := lstrProduction_no;
      if MyStringList[7] = '' then
        MyStringList[7] := MyStringList[6];
      if (MyStringList[0] <> '') and (MyStringList[1] <> '') and (MyStringList[2] <> '') and (MyStringList[3] <> '') and (MyStringList[4] <> '')
        and (MyStringList[5] <> '') and (MyStringList[6] <> '') and (MyStringList[7] <> '') and (MyStringList[8] <> '') then
      begin
        with qryReport do
        try
          Close;
          SQL.Clear;
          SQL.Add(Format(' insert into cue.MCPSMATCH_7 values(%s, %s, %s, %s, %s, %s, %s, %s, %s)',
          [QuotedStr(MyStringList[0]),QuotedStr(MyStringList[1]),
           MyStringList[2],QuotedStr(MyStringList[3]),
           QuotedStr(MyStringList[4]),QuotedStr(MyStringList[5]),
           QuotedStr(MyStringList[6]),QuotedStr(MyStringList[7]),
           MyStringList[8]]));
          Execute;
        except
          on E: Exception do
          begin
            ShowMessage('- WARN: - : '+lstrMatch+' '+E.Message);
            upRollback;
          end;
        end;
      end else
      begin
        Memo1.Lines.Add(MyStringList[0]+' : '+MyStringList[1]+' : '+MyStringList[2])
      end;
    end;
    ShowMessage('Data Uploaded');
    if cbCommit.Checked = True then
      upCommit
    else
      upRollback;
  finally
    CloseFile(MCPSMapFile);
    MyStringList.Free;
    Screen.Cursor := crDefault;
    SessionCUE.LogOff;
  end;

end;

procedure TExport.Button5Click(Sender: TObject);
var
  FlagFile : TextFile;
  lstrTxlogID : string;
  lintCount : integer;
  boolUpdate : boolean;
begin
  upConnectToDatabase;
  lintCount := 0;
  boolUpdate := True;
  if OpenDialog1.Execute then
  try
    AssignFile(FlagFile, OpenDialog1.FileName);
    Reset(FlagFile);
    while not EOF(FlagFile) do
    begin
      Readln(FlagFile, lstrTxlogID);
      with qryCheck do
      begin
        Close;
        SQL.Clear;
        SQL.Add(' Select ');
        case rgAgency.ItemIndex of
        0:
          begin
            SQL.Add(' CC_MCPS_REPORT_STATUS StatusUpdate ');
          end;
        1:
          begin
            SQL.Add(' CC_PPL_REPORT_STATUS StatusUpdate ');
          end;
        2:
          begin
            SQL.Add(' CC_VPL_REPORT_STATUS StatusUpdate ');
          end;
        3:
          begin
            SQL.Add(' CC_STIM_REPORT_STATUS StatusUpdate ');
          end;
        end;
        SQL.Add(format(' from CUE.TXLOG where ID = %s ',[lstrTxLogID]));
        execute;
        if not EOF then
        begin
          if FieldAsString('StatusUpdate') <> 'ST' then
          begin
            Memo1.Lines.Add(lstrTxLogID +' was not updated');
            boolUpdate := False;
            break;
          end else
            Inc(lintCount);
        end;

        eUpdateRowCount.Text := IntToStr(lintCount);

        Application.ProcessMessages;
      end;
    end;
    if boolUpdate = False then
      ShowMessage(' NOT ALL THE FLAGS HAVE BEEN UPDATED !!')
    else
      ShowMessage(' All flags have been updated');
  Finally
    CloseFile(FlagFile);
  end;

end;

procedure TExport.FormShow(Sender: TObject);
begin
  lblVersion.Caption := '1.0.1.1';
end;

procedure TExport.Button6Click(Sender: TObject);
var
  FlagFile : TextFile;
  lstrTxlogID : string;
  lintCount : integer;
  boolUpdate : boolean;
  lstrlChannel : TStringList;
  lstrlBroadcaster : TStringList;
begin
  upConnectToDatabase;
  lintCount := 0;
  boolUpdate := True;
  if OpenDialog1.Execute then
  try
    lstrlChannel := TStringList.Create;
    lstrlChannel.Sorted := True;
    lstrlChannel.Duplicates := dupIgnore;

    lstrlBroadcaster := TStringList.Create;
    lstrlBroadcaster.Sorted := True;
    lstrlBroadcaster.Duplicates := dupIgnore;

    AssignFile(FlagFile, OpenDialog1.FileName);
    Reset(FlagFile);
    while not EOF(FlagFile) do
    begin
      Readln(FlagFile, lstrTxlogID);
      Inc(lintCount);
      eUpdateRowCount.Text := IntToStr(lintCount);
      Application.ProcessMessages;

      with qryCheck do
      begin
        Close;
        SQL.Clear;
        SQL.Add(Format(' Select CHANNEL_ID, CH.NAME CHANNELNAME, BROADCASTER_ID from CUE.TXLOG TXL, CUE.CHANNEL CH '+
                       ' where TXL.ID = %s and TXL.CHANNEL_ID = CH.ID',[lstrTxlogID]));
        Execute;
        if not EOF then
        begin
          lstrlChannel.Add(FieldAsString('CHANNELNAME'));
          lstrlBroadcaster.Add(FieldAsString('BROADCASTER_ID'));
        end;
      end;
    end;
    Memo1.Lines.Add('');
    Memo1.Lines.Add('The Following list of Channels were found in the Txlog Update List');
    Memo1.Lines.Add(lstrlChannel.CommaText);
    Memo1.Lines.Add('');
    Memo1.Lines.Add('The following list of Broadcasters were found in the Txlog Update List');
    Memo1.Lines.Add(lstrlBroadcaster.CommaText);
  finally
    lstrlChannel.Free;
  end;

end;

end.


