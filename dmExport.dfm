object Export: TExport
  Left = 208
  Top = 197
  Width = 562
  Height = 695
  Caption = 'Export'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 48
    Width = 53
    Height = 13
    Caption = 'Channel ID'
  end
  object Label2: TLabel
    Left = 24
    Top = 16
    Width = 77
    Height = 13
    Caption = 'Database Name'
  end
  object Label3: TLabel
    Left = 25
    Top = 75
    Width = 71
    Height = 13
    Caption = 'Rows Updated'
  end
  object Label4: TLabel
    Left = 33
    Top = 338
    Width = 70
    Height = 13
    Caption = 'Date Reported'
  end
  object Label5: TLabel
    Left = 283
    Top = 339
    Width = 58
    Height = 13
    Caption = 'dd/mm/yyyy'
  end
  object Label6: TLabel
    Left = 16
    Top = 128
    Width = 35
    Height = 13
    Caption = 'Version'
  end
  object lblVersion: TLabel
    Left = 64
    Top = 128
    Width = 45
    Height = 13
    Caption = 'lblVersion'
  end
  object Button1: TButton
    Left = 128
    Top = 391
    Width = 202
    Height = 25
    Caption = 'Update Flags'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 0
    Top = 500
    Width = 546
    Height = 157
    Align = alBottom
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object eChannelID: TEdit
    Left = 128
    Top = 40
    Width = 33
    Height = 21
    TabOrder = 2
  end
  object eDatabase: TEdit
    Left = 128
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object cbCommit: TCheckBox
    Left = 24
    Top = 104
    Width = 97
    Height = 17
    Caption = 'Commit'
    TabOrder = 4
  end
  object rgAgency: TRadioGroup
    Left = 128
    Top = 176
    Width = 201
    Height = 129
    Caption = 'Select Society'
    Columns = 2
    Items.Strings = (
      'MCPS'
      'PPL'
      'VPL'
      'STIM')
    TabOrder = 5
  end
  object eUpdateRowCount: TEdit
    Left = 128
    Top = 72
    Width = 121
    Height = 21
    TabOrder = 6
  end
  object Button5: TButton
    Left = 128
    Top = 455
    Width = 202
    Height = 25
    Caption = 'Check Flags have been updated'
    TabOrder = 7
    OnClick = Button5Click
  end
  object eDateReported: TEdit
    Left = 128
    Top = 336
    Width = 145
    Height = 21
    TabOrder = 8
  end
  object Button6: TButton
    Left = 128
    Top = 423
    Width = 202
    Height = 25
    Caption = 'Get list of channels from Update List'
    TabOrder = 9
    OnClick = Button6Click
  end
  object cbUseFileNameDate: TCheckBox
    Left = 129
    Top = 360
    Width = 169
    Height = 17
    Caption = 'Use Filename Date'
    TabOrder = 10
  end
  object cbRunUpdates: TCheckBox
    Left = 128
    Top = 104
    Width = 97
    Height = 17
    Caption = 'Run Updates'
    TabOrder = 11
  end
  object OracleLogon1: TOracleLogon
    Left = 360
    Top = 48
  end
  object SessionCUE: TOracleSession
    LogonUsername = 'CUE'
    LogonPassword = 'EO2KAENW'
    LogonDatabase = 'CUETEST'
    Left = 360
    Top = 16
  end
  object qryReport: TOracleQuery
    Session = SessionCUE
    Optimize = False
    Left = 448
    Top = 16
  end
  object qryGen: TOracleQuery
    Session = SessionCUE
    Optimize = False
    Left = 400
    Top = 16
  end
  object qryRollback: TOracleQuery
    Session = SessionCUE
    Optimize = False
    Left = 400
    Top = 56
  end
  object qryCommit: TOracleQuery
    Session = SessionCUE
    Optimize = False
    Left = 448
    Top = 56
  end
  object OpenDialog1: TOpenDialog
    Left = 360
    Top = 96
  end
  object qryCheck: TOracleQuery
    Session = SessionCUE
    Optimize = False
    Left = 408
    Top = 96
  end
end
